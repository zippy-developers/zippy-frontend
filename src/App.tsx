import { Suspense, lazy, useEffect, useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import Register from './pages/Authentication/Register';
import Login from './pages/Authentication/Login';
import Terms from './pages/Terms';
import Privacy from './pages/PrivacyPolicy';
import ForgotPassword from './pages/Authentication/ForgotPassword';
import ResetPassword from './pages/Authentication/ResetPassword';
import { Toaster } from 'react-hot-toast';

import { useAuth } from './context/AuthContext';
import Loader from './common/Loader';
import ECommerce from './pages/Dashboard/ECommerce';
import SendGoods from './pages/Delivery/SendGoods';
import ReceiveGoods from './pages/Delivery/RecieveGoods';
import ViewSendGoods from './pages/Delivery/ViewSendGoods';
import ViewReceiveGoods from './pages/Delivery/ViewReceiveGoods';
const Calendar = lazy(() => import('./pages/Calendar'));
const Chart = lazy(() => import('./pages/Chart'));
const FormElements = lazy(() => import('./pages/Form/FormElements'));
const FormLayout = lazy(() => import('./pages/Form/FormLayout'));
const Profile = lazy(() => import('./pages/Profile'));
const CreateRequest = lazy(() => import('./pages/Delivery/CreateRequest'));
const ViewRequest = lazy(() => import('./pages/Delivery/ViewRequest'));
const Settings = lazy(() => import('./pages/Settings'));
const Tables = lazy(() => import('./pages/Tables'));
const Alerts = lazy(() => import('./pages/UiElements/Alerts'));
const Buttons = lazy(() => import('./pages/UiElements/Buttons'));
const DefaultLayout = lazy(() => import('./layout/DefaultLayout'));

function App() {
  const [loading, setLoading] = useState<boolean>(true);
  const { state } = useAuth();

  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
  }, []);

  return loading ? (
    <Loader />
  ) : (
    <>
      <Toaster
        position='top-right'
        toastOptions={{
          style: {
            background: '#363636',
            color: '#fff',
          },
        }}
      />
      {state.user ? (
        <Routes>
          <Route element={<DefaultLayout />}>
            <Route index element={<ECommerce />} />
            <Route
              path='/calendar'
              element={
                <Suspense fallback={<Loader />}>
                  <Calendar />
                </Suspense>
              }
            />
            <Route
              path='/profile'
              element={
                <Suspense fallback={<Loader />}>
                  <Profile />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/create-request'
              element={
                <Suspense fallback={<Loader />}>
                  <CreateRequest />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/create-request/send-goods'
              element={
                <Suspense fallback={<Loader />}>
                  <SendGoods />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/create-request/receive-goods'
              element={
                <Suspense fallback={<Loader />}>
                  <ReceiveGoods />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/view-requests'
              element={
                <Suspense fallback={<Loader />}>
                  <ViewRequest />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/view-requests/view-send-goods'
              element={
                <Suspense fallback={<Loader />}>
                  <ViewSendGoods />
                </Suspense>
              }
            />
            <Route
              path='delivery-request/view-requests/view-receive-goods'
              element={
                <Suspense fallback={<Loader />}>
                  <ViewReceiveGoods />
                </Suspense>
              }
            />
            <Route
              path='/forms/form-elements'
              element={
                <Suspense fallback={<Loader />}>
                  <FormElements />
                </Suspense>
              }
            />
            <Route
              path='/forms/form-layout'
              element={
                <Suspense fallback={<Loader />}>
                  <FormLayout />
                </Suspense>
              }
            />
            <Route
              path='/tables'
              element={
                <Suspense fallback={<Loader />}>
                  <Tables />
                </Suspense>
              }
            />
            <Route
              path='/settings'
              element={
                <Suspense fallback={<Loader />}>
                  <Settings />
                </Suspense>
              }
            />
            <Route
              path='/chart'
              element={
                <Suspense fallback={<Loader />}>
                  <Chart />
                </Suspense>
              }
            />
            <Route
              path='/ui/alerts'
              element={
                <Suspense fallback={<Loader />}>
                  <Alerts />
                </Suspense>
              }
            />
            <Route
              path='/ui/buttons'
              element={
                <Suspense fallback={<Loader />}>
                  <Buttons />
                </Suspense>
              }
            />
          </Route>
          <Route path='/terms' element={<Terms />} />
          <Route path='/privacy-policy' element={<Privacy />} />
        </Routes>
      ) : (
        <Routes>
          <Route path='/auth/login' element={<Login />} />
          <Route path='/auth/register' element={<Register />} />
          <Route path='/terms' element={<Terms />} />
          <Route path='/privacy-policy' element={<Privacy />} />
          <Route path='/auth/forgot-password' element={<ForgotPassword />} />
          <Route
            path='/auth/reset-password/:id/:token'
            element={<ResetPassword />}
          />
          <Route path='*' element={<Navigate to='/auth/login' replace />} />
        </Routes>
      )}
    </>
  );
}

export default App;
