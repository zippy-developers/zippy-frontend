/// <reference types="react-scripts" />

interface ImportMetaEnv {
  VITE_API_URL: string;
  // Add other environment variables here
}

// Extend the existing ImportMeta type
interface ImportMeta {
  env: ImportMetaEnv;
}
