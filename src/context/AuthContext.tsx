import React, {
  createContext,
  useContext,
  useReducer,
  ReactNode,
  useEffect,
} from 'react';

// Define the keys for localStorage
const USER_KEY = 'authUser';

// Replace 'any' with your actual user and error types
interface User {
  id: string;
  username: string;
  // Add other user-related properties here
}

interface AuthState {
  user: User | null;
  loading: boolean;
  error: Error | null;
}

type AuthAction =
  | { type: 'LOGIN_START' }
  | { type: 'LOGIN_SUCCESS'; payload: User }
  | { type: 'LOGIN_FAILURE'; payload: Error }
  | { type: 'LOGOUT' };



type AuthDispatch = (action: AuthAction) => void;

const AuthContext = createContext<
  { state: AuthState; dispatch: AuthDispatch } | undefined
>(undefined);

const authReducer = (state: AuthState, action: AuthAction): AuthState => {
  switch (action.type) {
    case 'LOGIN_START':
      return { ...state, loading: true, error: null };
    case 'LOGIN_SUCCESS':
      localStorage.setItem(USER_KEY, JSON.stringify(action.payload));
      return { ...state, loading: false, user: action.payload, error: null };
    case 'LOGIN_FAILURE':
      return { ...state, loading: false, error: action.payload };
    case 'LOGOUT':
      localStorage.removeItem('color-theme');
      localStorage.removeItem(USER_KEY);
      window.location.reload();
      return { ...state, user: null, loading: true, error: null };
    default:
      return state;
  }
};

const AuthProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, {
    user: JSON.parse(localStorage.getItem(USER_KEY) || 'null'),
    loading: false,
    error: null,
  });

  useEffect(() => {
    try {
      const storedUser = JSON.parse(localStorage.getItem(USER_KEY) || 'null');
      if (state.user !== storedUser) {
        dispatch({ type: 'LOGIN_SUCCESS', payload: storedUser });
      }
    } catch (error) {
      // Handle parsing errors if necessary
      console.error('Error parsing user data from localStorage:', error);
    }
  }, []);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};

export { AuthProvider, useAuth };

export const logout = (dispatch: AuthDispatch) => {
  dispatch({ type: 'LOGOUT' });
};
