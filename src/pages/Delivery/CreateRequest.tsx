import { useNavigate } from 'react-router-dom';
import Breadcrumb from '../../components/Breadcrumb';

const CreateRequest = () => {
  const navigate = useNavigate();

  const handleSendGoods = () => {
    navigate('send-goods');
  };

  const handleReceiveGoods = () => {
    navigate('receive-goods');
  };

  return (
    <>
      <Breadcrumb pageName='Create Request' />

      <div className='flex h-[77vh]'>
        <div
          className='flex items-center justify-center flex-1 transition-all duration-500 cursor-pointer hover:bg-bodydark1 dark:hover:bg-boxdark'
          onClick={handleSendGoods}
        >
          <button className='rounded-full px-4 py-2 text-[20px] font-bold text-black dark:text-white'>
            Send Goods
          </button>
        </div>
        <div
          className='flex items-center justify-center flex-1 transition-all duration-500 cursor-pointer hover:bg-bodydark1 dark:hover:bg-boxdark'
          onClick={handleReceiveGoods}
        >
          <button className='rounded-full px-4 py-2 text-[20px] font-bold text-black dark:text-white'>
            Receive Goods
          </button>
        </div>
      </div>
    </>
  );
};

export default CreateRequest;
