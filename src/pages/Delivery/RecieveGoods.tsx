import axios from 'axios';
import Breadcrumb from '../../components/Breadcrumb';
import { useEffect, useState } from 'react';
import toast from 'react-hot-toast';

const apiUrl = import.meta.env.VITE_API_URL;

const ReceiveGoods = () => {
  const preset_key = 'l6sgpysj';
  const cloud_name = 'dynufetih';

  const currentDate = new Date().toISOString().substring(0, 10);

  const user = JSON.parse(localStorage.getItem('authUser') || '');

  const [countries, setCountries]: any = useState([]);
  const [errors, setErrors] = useState({});
  const [selectedImage, setSelectedImage] = useState(null);

  const [formData, setFormData] = useState({
    userID: user.details._id,
    receiverName: user.details.firstName + ' ' + user.details.lastName,
    receiverEmail: user.details.email,
    receiverPhone: user.details.phone,
    senderName: '',
    senderAddress: '',
    senderState: '',
    senderPostalCode: '',
    senderCountry: '',
    senderEmail: '',
    senderPhone: '',
    orderCategory: '',
    orderSize: '',
    orderImage: '',
    orderWeight: '',
    orderReceiveDate: '',
    orderPlaceDate: currentDate,
    orderCost: '',
    status: 'pending',
  });

  // console.log(formData);

  const resetForm = () => {
    setFormData({
      userID: user.details._id,
      receiverName: '',
      receiverEmail: '',
      receiverPhone: '',
      senderName: '',
      senderAddress: '',
      senderState: '',
      senderPostalCode: '',
      senderCountry: '',
      senderEmail: '',
      senderPhone: '',
      orderCategory: '',
      orderSize: '',
      orderImage: '',
      orderWeight: '',
      orderReceiveDate: '',
      orderPlaceDate: currentDate,
      orderCost: '',
      status: 'pending',
    });

    setSelectedImage(null);
  };

  const calculateOrderCost = () => {
    const { orderCategory, orderSize, orderWeight } = formData;
    let cost = 0;

    switch (orderCategory) {
      case 'Food & Beverages':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.2;
        break;

      case 'Grocery & Essentials':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.15;
        break;

      case 'Electronics':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.3;
        break;

      case 'Fashion & Apparel':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.25;
        break;

      case 'Home & Garden':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.35;
        break;

      case 'Health & Wellness':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.18;
        break;

      case 'Books & Entertainment':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.22;
        break;

      case 'Office Supplies':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.16;
        break;

      case 'Automotive':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.28;
        break;

      case 'Pet Supplies':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.17;
        break;

      case 'Sporting Goods':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.25;
        break;

      case 'Toys & Games':
        cost = parseFloat(orderSize) * parseFloat(orderWeight) * 0.2;
        break;

      default:
        cost = 0;
    }

    const exchangeRate = 320;

    const costInLKR = cost * exchangeRate;

    return costInLKR.toFixed(2);
  };

  useEffect(() => {
    // Fetch the list of countries from the Restcountries API
    fetch('https://restcountries.com/v3.1/all')
      .then((response) => response.json())
      .then((data) => {
        // Extract country names from the API response
        const countryNames: any = data.map(
          (country: any) => country.name.common
        );
        // Sort the country names in ascending alphabetical order
        countryNames.sort((a: any, b: any) => a.localeCompare(b));
        // Add "Select your country" as the first option
        setCountries(['Select your country', ...countryNames]);
      })
      .catch((error) => {
        console.error('Error fetching countries:', error);
      });
  }, []);

  const handleInputChange = (event: any) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));

    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: '',
    }));

    if (
      name === 'orderSize' ||
      name === 'orderWeight' ||
      name === 'orderCategory'
    ) {
      const cost = calculateOrderCost();
      setFormData((prevData) => ({
        ...prevData,
        orderCost: cost,
      }));
    }
  };

  const handleDropdownChange = (e: any) => {
    e.preventDefault();
    const { value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      orderCategory: value,
    }));

    const cost: any = calculateOrderCost();
    setFormData((prevData) => ({
      ...prevData,
      orderCost: cost,
    }));
  };

  useEffect(() => {
    const cost = calculateOrderCost();
    setFormData((prevData) => ({
      ...prevData,
      orderCost: cost,
    }));
  }, [formData.orderCategory, formData.orderSize, formData.orderWeight]);

  const handleImageUpload = async (e: any) => {
    const file = e.target.files[0];

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', preset_key);

    try {
      const uploadResponse = await axios.post(
        `https://api.cloudinary.com/v1_1/${cloud_name}/image/upload`,
        formData
      );
      const imageURL = uploadResponse.data.secure_url;

      setSelectedImage(imageURL);

      setFormData((preData) => ({
        ...preData,
        orderImage: imageURL,
      }));
    } catch (error) {
      console.error('Error uploading image:', error);
    }
  };

  const validateForm = () => {
    let isValid = true;

    if (!formData.senderName) {
      isValid = false;
    }

    if (!formData.senderEmail) {
      isValid = false;
    } else if (!/^\S+@\S+\.\S+$/.test(formData.senderEmail)) {
      isValid = false;
    }

    if (!formData.senderPhone) {
      isValid = false;
    } else if (!/^[0-9]{10}$/g.test(formData.senderPhone)) {
      isValid = false;
    }

    if (!formData.senderAddress) {
      isValid = false;
    }

    if (!formData.senderState) {
      isValid = false;
    }

    if (!formData.senderPostalCode) {
      isValid = false;
    }

    if (formData.senderCountry === 'Select your country') {
      isValid = false;
    }

    if (!formData.orderSize) {
      isValid = false;
    } else if (parseFloat(formData.orderSize) <= 0) {
      isValid = false;
    }

    if (!formData.orderWeight) {
      isValid = false;
    } else if (parseFloat(formData.orderWeight) <= 0) {
      isValid = false;
    }

    if (!formData.orderReceiveDate) {
      isValid = false;
    }

    setErrors(errors);

    return isValid;
  };

  const handlePlaceOrder = async (e: any) => {
    e.preventDefault();

    const isValid = validateForm();

    if (isValid) {
      try {
        const response = await axios.post(`${apiUrl}/receive-goods`, formData, {
          headers: {
            'Content-Type': 'application/json',
          },
        });

        if (response.status === 201) {
          toast.success('Receive goods request successful');
          resetForm();
          setSelectedImage(null);
        } else {
          toast.error('Error delivery request');
        }
      } catch (error) {
        toast.error('An error occurred while delivery request');
      }
    } else {
      toast.error('Please fill the form correctly.');
    }
  };

  return (
    <>
      <Breadcrumb pageName='Receive Goods' />

      <form onSubmit={handlePlaceOrder}>
        <div className='grid grid-cols-1 gap-9 sm:grid-cols-2'>
          <div className='flex-col gap-9'>
            <div className='bg-white border rounded-sm border-stroke shadow-default dark:border-strokedark dark:bg-boxdark'>
              <div className='border-b border-stroke px-6.5 py-4 dark:border-strokedark'>
                <h3 className='font-medium text-black dark:text-white'>
                  Sender's Details
                </h3>
              </div>
              <div className='p-6.5'>
                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Sender's Name <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='text'
                    id='senderName'
                    name='senderName'
                    value={formData.senderName}
                    onChange={handleInputChange}
                    required
                    placeholder="Enter your sender's name"
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Sender's Email <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='email'
                    id='senderEmail'
                    name='senderEmail'
                    value={formData.senderEmail}
                    onChange={handleInputChange}
                    required
                    placeholder='Enter your email address'
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Sender's Phone <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='phone'
                    id='senderPhone'
                    name='senderPhone'
                    value={formData.senderPhone}
                    onChange={handleInputChange}
                    required
                    placeholder='Enter your phone number'
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Sender's Address <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='text'
                    id='senderAddress'
                    name='senderAddress'
                    value={formData.senderAddress}
                    onChange={handleInputChange}
                    required
                    placeholder='Enter your address'
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5 flex flex-col gap-6 xl:flex-row'>
                  <div className='w-full xl:w-1/2'>
                    <label className='mb-2.5 block text-black dark:text-white'>
                      Sender's State <span className='text-meta-1'>*</span>
                    </label>
                    <input
                      type='text'
                      id='senderState'
                      name='senderState'
                      value={formData.senderState}
                      onChange={handleInputChange}
                      required
                      placeholder='Enter state'
                      className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                    />
                  </div>

                  <div className='w-full xl:w-1/2'>
                    <label className='mb-2.5 block text-black dark:text-white'>
                      Sender's Postal Code{' '}
                      <span className='text-meta-1'>*</span>
                    </label>
                    <input
                      type='number'
                      id='senderPostalCode'
                      name='senderPostalCode'
                      value={formData.senderPostalCode}
                      onChange={handleInputChange}
                      required
                      placeholder='Enter postal code'
                      className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                    />
                  </div>
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Sender's Country <span className='text-meta-1'>*</span>
                  </label>
                  <div className='relative z-20 bg-transparent dark:bg-form-input'>
                    <select
                      className='relative z-20 w-full px-5 py-3 transition bg-transparent border rounded outline-none appearance-none border-stroke focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                      id='senderCountry'
                      name='senderCountry'
                      value={formData.senderCountry}
                      onChange={handleInputChange}
                      required
                    >
                      {countries.map((country: any, index: any) => (
                        <option key={index} value={country}>
                          {country}
                        </option>
                      ))}
                    </select>
                    <span className='absolute z-30 -translate-y-1/2 right-4 top-1/2'>
                      <svg
                        className='fill-current'
                        width='24'
                        height='24'
                        viewBox='0 0 24 24'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <g opacity='0.8'>
                          <path
                            fillRule='evenodd'
                            clipRule='evenodd'
                            d='M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z'
                            fill=''
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className='flex-col gap-9'>
            {/* <!-- Order Details --> */}
            <div className='bg-white border rounded-sm border-stroke shadow-default dark:border-strokedark dark:bg-boxdark'>
              <div className='border-b border-stroke px-6.5 py-4 dark:border-strokedark'>
                <h3 className='font-medium text-black dark:text-white'>
                  Order Details
                </h3>
              </div>
              <div className='p-6.5'>
                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Category <span className='text-meta-1'>*</span>
                  </label>
                  <div className='relative z-20 bg-transparent dark:bg-form-input'>
                    <select
                      className='relative z-20 w-full px-5 py-3 transition bg-transparent border rounded outline-none appearance-none border-stroke focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                      id='productCategory'
                      name='productCategory'
                      onChange={handleDropdownChange}
                    >
                      <option value=''>Select a category</option>
                      <option value='Food & Beverages'>Food & Beverages</option>
                      <option value='Grocery & Essentials'>
                        Grocery & Essentials
                      </option>
                      <option value='Electronics'>Electronics</option>
                      <option value='Fashion & Apparel'>
                        Fashion & Apparel
                      </option>
                      <option value='Home & Garden'>Home & Garden</option>
                      <option value='Health & Wellness'>
                        Health & Wellness
                      </option>
                      <option value='Books & Entertainment'>
                        Books & Entertainment
                      </option>
                      <option value='Office Supplies'>Office Supplies</option>
                      <option value='Automotive'>Automotive</option>
                      <option value='Pet Supplies'>Pet Supplies</option>
                      <option value='Sporting Goods'>Sporting Goods</option>
                      <option value='Toys & Games'>Toys & Games</option>
                    </select>
                    <span className='absolute z-30 -translate-y-1/2 right-4 top-1/2'>
                      <svg
                        className='fill-current'
                        width='24'
                        height='24'
                        viewBox='0 0 24 24'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <g opacity='0.8'>
                          <path
                            fillRule='evenodd'
                            clipRule='evenodd'
                            d='M5.29289 8.29289C5.68342 7.90237 6.31658 7.90237 6.70711 8.29289L12 13.5858L17.2929 8.29289C17.6834 7.90237 18.3166 7.90237 18.7071 8.29289C19.0976 8.68342 19.0976 9.31658 18.7071 9.70711L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L5.29289 9.70711C4.90237 9.31658 4.90237 8.68342 5.29289 8.29289Z'
                            fill=''
                          ></path>
                        </g>
                      </svg>
                    </span>
                  </div>
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Quantity <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='number'
                    id='orderSize'
                    name='orderSize'
                    value={formData.orderSize}
                    onChange={handleInputChange}
                    placeholder='Enter order size'
                    required
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Weight <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='number'
                    id='orderWeight'
                    name='orderWeight'
                    value={formData.orderWeight}
                    onChange={handleInputChange}
                    placeholder='Enter order weight'
                    required
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Receive Date <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='date'
                    id='orderReceiveDate'
                    name='orderReceiveDate'
                    value={formData.orderReceiveDate}
                    onChange={handleInputChange}
                    placeholder='Enter receive date'
                    required
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Image <span className='text-meta-1'>*</span>
                  </label>
                  <input
                    type='file'
                    id='orderImage'
                    name='orderImage'
                    accept='image/*'
                    onChange={handleImageUpload}
                    className='w-full px-5 py-3 font-medium transition bg-transparent border outline-none border-stroke focus:border-primary active:border-primary dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                {selectedImage && (
                  <div className='mb-4.5'>
                    <label className='mb-2.5 block text-black dark:text-white'>
                      Order Image Preview <span className='text-meta-1'>*</span>
                    </label>
                    <img
                      src={selectedImage}
                      alt='Selected Image'
                      className='object-cover w-full rounded'
                    />
                  </div>
                )}

                <div className='mb-4.5'>
                  <label className='mb-2.5 block text-black dark:text-white'>
                    Order Cost
                  </label>
                  <input
                    type='text'
                    id='orderCost'
                    name='orderCost'
                    value={
                      formData.orderCost !== ''
                        ? `LKR ${formData.orderCost}`
                        : ''
                    }
                    readOnly
                    className='w-full rounded border-[1.5px] border-stroke bg-transparent px-5 py-3 font-medium outline-none transition focus:border-primary active:border-primary disabled:cursor-default disabled:bg-whiter dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                  />
                </div>

                <button
                  className='flex justify-center w-full p-3 font-medium rounded bg-primary text-gray'
                  type='submit'
                  onClick={handlePlaceOrder}
                >
                  Place Order
                </button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default ReceiveGoods;
