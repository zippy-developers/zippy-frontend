import { useNavigate } from 'react-router-dom';
import Breadcrumb from '../../components/Breadcrumb';

const ViewRequest = () => {
  const navigate = useNavigate();

  const handleSendGoods = () => {
    navigate('view-send-goods');
  };

  const handleReceiveGoods = () => {
    navigate('view-receive-goods');
  };

  return (
    <>
      <Breadcrumb pageName='View Requests' />

      <div className='flex h-[77vh]'>
        <div
          className='flex items-center justify-center flex-1 transition-all duration-500 cursor-pointer hover:bg-bodydark1 dark:hover:bg-boxdark'
          onClick={handleSendGoods}
        >
          <button className='rounded-full px-4 py-2 text-[20px] font-bold text-black dark:text-white'>
            View Send Goods
          </button>
        </div>
        <div
          className='flex items-center justify-center flex-1 transition-all duration-500 cursor-pointer hover:bg-bodydark1 dark:hover:bg-boxdark'
          onClick={handleReceiveGoods}
        >
          <button className='rounded-full px-4 py-2 text-[20px] font-bold text-black dark:text-white'>
            View Receive Goods
          </button>
        </div>
      </div>
    </>
  );
};

export default ViewRequest;
