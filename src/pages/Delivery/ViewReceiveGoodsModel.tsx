const ViewReceiveGoodsModel = ({ request, onClose }: any) => {
  return (
    <div className='overlay'>
      <div className='fixed inset-0 z-50 flex items-center justify-center'>
        <div className='flex max-w-3xl p-8 bg-white rounded-lg shadow-md'>
          <div className='w-1/3 pr-6'>
            {/* Left column for image */}
            <img
              src={request.orderImage} // Replace with the actual image source
              alt='Delivery Request'
              className='w-full h-auto rounded-md'
            />
          </div>
          <div className='w-2/3'>
            {/* Right column for details */}
            <h2 className='mb-4 text-xl font-semibold'>
              View Delivery Request
            </h2>
            <div>
              <p>
                <strong>Sender Name:</strong> {request.senderName}
              </p>
              <p>
                <strong>Category:</strong> {request.orderCategory}
              </p>
              <p>
                <strong>Sender Address:</strong> {request.senderAddress}
              </p>
              <p>
                <strong>Invoice Date:</strong> {request.orderPlaceDate}
              </p>
              <p>
                <strong>Status:</strong> {request.status}
              </p>
              {/* Add more fields as needed */}
            </div>
            <div className='flex justify-end mt-4'>
              <button
                className='inline-flex items-center justify-center px-10 py-4 font-medium text-center text-white rounded-md bg-primary hover:bg-opacity-90 lg:px-8 xl:px-10'
                onClick={onClose}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewReceiveGoodsModel;
