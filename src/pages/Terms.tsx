import React from 'react';
import { RxArrowLeft } from 'react-icons/rx';

const Terms = () => {
  return (
    <div className='mx-auto flex flex-col items-center justify-center bg-primary py-20'>
      <div>
        <div className='flex cursor-pointer items-center justify-end gap-2 py-5 text-white'>
          <span>
            <RxArrowLeft />
          </span>
          <button
            type='button'
            onClick={() => window.history.back() as any}
            className='rounded-lg border border-primary bg-primary  text-white transition hover:bg-opacity-90'
          >
            Back
          </button>
        </div>
        <div className='rounded-xl border border-stroke bg-white p-18 shadow-default dark:border-strokedark dark:bg-boxdark sm:w-auto md:w-auto lg:w-auto'>
          <div className='mx-auto max-w-3xl text-black dark:text-white'>
            <h1 className='mb-4 text-3xl font-bold'>
              Zippy Delivery Service - Terms and Conditions
            </h1>
            <p>Effective Date: 2023/10/12</p>

            <p className='mt-4'>
              <strong>1. Acceptance of Terms</strong>
            </p>
            <p>
              By using the Zippy Delivery Service website, you agree to comply
              with and be bound by these Terms and Conditions. If you do not
              agree to these terms, please do not use the service.
            </p>

            <p className='mt-4'>
              <strong>2. Service Description</strong>
            </p>
            <p>
              Zippy Delivery Service provides an online platform for customers
              to request deliveries to recipients of their choice. Customers can
              place delivery requests online through our website, track
              deliveries, and manage their account.
            </p>

            <p className='mt-4'>
              <strong>3. Account Registration</strong>
            </p>
            <p>
              To use our services, you must create an account. You agree to
              provide accurate, current, and complete information during the
              registration process and to update such information to keep it
              accurate, current, and complete.
            </p>

            <p className='mt-4'>
              <strong>4. Delivery Requests</strong>
            </p>
            <p>
              Customers can place delivery requests through our website,
              specifying pickup and delivery details. Zippy Delivery Service
              will make reasonable efforts to complete deliveries within the
              specified timeframes, but we do not guarantee delivery times.
              Customers must ensure that the recipient is available at the
              specified delivery location.
            </p>

            <p className='mt-4'>
              <strong>5. User Responsibilities</strong>
            </p>
            <p>5.1. You agree to use the service for lawful purposes only.</p>
            <p>
              5.2. You are responsible for the accuracy of information provided
              in delivery requests, including recipient contact details and item
              descriptions.
            </p>
            <p>
              5.3. You are responsible for any costs associated with failed
              delivery attempts due to recipient unavailability or incorrect
              information.
            </p>

            <p className='mt-4'>
              <strong>6. Service Fees and Payments</strong>
            </p>
            <p>6.1. Service fees will apply as specified on our website.</p>
            <p>
              6.2. You agree to pay all fees associated with the services, which
              are due upon confirmation of your delivery request.
            </p>

            <p className='mt-4'>
              <strong>7. Privacy Policy</strong>
            </p>
            <p>
              Our Privacy Policy is incorporated into these Terms and Conditions
              and is available on our website. By using our service, you consent
              to the collection and use of your information as described in our
              Privacy Policy.
            </p>

            <p className='mt-4'>
              <strong>8. Termination</strong>
            </p>
            <p>
              We reserve the right to suspend or terminate your account or
              access to our service at our discretion, without notice, for any
              violation of these Terms and Conditions.
            </p>

            <p className='mt-4'>
              <strong>9. Limitation of Liability</strong>
            </p>
            <p>
              Zippy Delivery Service is not liable for any damages, losses, or
              costs incurred as a result of using our service. We are not
              responsible for the content of items delivered, their condition,
              or any issues arising from the delivery process.
            </p>

            <p className='mt-4'>
              <strong>10. Changes to Terms</strong>
            </p>
            <p>
              We may revise these Terms and Conditions at any time, and such
              revisions will be effective upon posting. You should periodically
              review these terms to stay informed of any changes.
            </p>

            <p className='mt-4'>
              <strong>11. Contact Information</strong>
            </p>
            <p>
              For questions or concerns about these Terms and Conditions, please
              contact us at contact@zippy.lk.
            </p>

            <p className='mt-4'>
              By using Zippy Delivery Service, you acknowledge that you have
              read, understood, and agree to be bound by these Terms and
              Conditions.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Terms;
