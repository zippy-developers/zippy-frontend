import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import LogoDark from '../../images/logo/logo-dark.svg';
import Logo from '../../images/logo/logo.svg';
import axios from 'axios';
import { useAuth } from '../../context/AuthContext';
import toast from 'react-hot-toast';
import Loader from '../../common/Loader';

const apiUrl = import.meta.env.VITE_API_URL;
const Login = () => {
  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });

  const [loading, setLoading] = useState(false);
  const { state, dispatch } = useAuth();
  const navigate = useNavigate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    setCredentials((prev) => ({ ...prev, [id]: value }));
  };

  const handleClick = async (e: React.FormEvent) => {
    e.preventDefault();
    dispatch({ type: 'LOGIN_START' });
    setLoading(true);

    try {
      const response = await axios.post(`${apiUrl}/auth/login`, credentials);
      const user = response.data;
      dispatch({ type: 'LOGIN_SUCCESS', payload: user });
      navigate('/');
    } catch (err: any) {
      dispatch({ type: 'LOGIN_FAILURE', payload: err.response.data });
      toast.error('Login failed. Please check your credentials.');
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      {state.loading ? (
        <Loader />
      ) : (
        <div className='mx-auto flex flex-col items-center justify-center bg-primary min-[420px]:h-screen sm:h-screen md:h-screen lg:h-screen'>
          <div className='rounded-xl border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark sm:w-[600px] md:w-[700px] lg:w-[1000px]'>
            <div className='flex flex-wrap items-center'>
              <div className='hidden w-full xl:block xl:w-1/2'>
                <div className='py-8 text-center px-26'>
                  <Link className='mb-5.5 inline-block' to='/'>
                    <img
                      className='hidden w-full dark:block'
                      src={LogoDark}
                      alt='Logo'
                    />
                    <img className='w-full dark:hidden' src={Logo} alt='Logo' />
                  </Link>

                  <p className='2xl:px-2'>
                    Ready to deliver at any time of the day or night.
                  </p>
                </div>
              </div>

              <div className='w-full border-stroke dark:border-strokedark xl:w-1/2 xl:border-l-2'>
                <div className='w-full p-4 sm:p-12.5 xl:p-17.5'>
                  <span className='mb-1.5 block font-medium'>
                    Start for free
                  </span>
                  <h2 className='text-2xl font-bold text-black mb-9 dark:text-white sm:text-title-xl2'>
                    Login to Zippy
                  </h2>

                  <form>
                    <div className='mb-4'>
                      <label className='mb-2.5 block font-medium text-black dark:text-white'>
                        Username
                      </label>
                      <div className='relative'>
                        <input
                          type='text'
                          id='username'
                          name='username'
                          onChange={handleChange}
                          placeholder='Enter your username'
                          className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                        />
                      </div>
                    </div>

                    <div className='mb-6'>
                      <label className='mb-2.5 block font-medium text-black dark:text-white'>
                        Password
                      </label>
                      <div className='relative'>
                        <input
                          type='password'
                          id='password'
                          name='password'
                          onChange={handleChange}
                          placeholder='6+ Characters, 1 Capital letter'
                          className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                        />
                      </div>
                    </div>

                    <div className='mb-5 text-primary'>
                      <Link to='/auth/forgot-password'>Forgot Password?</Link>
                    </div>

                    <div className='mb-5'>
                      <button
                        onClick={handleClick}
                        type='submit'
                        value='Sign In'
                        className='w-full p-4 text-white transition border rounded-lg cursor-pointer border-primary bg-primary hover:bg-opacity-90'
                      >
                        Login
                      </button>
                    </div>
                    <div className='mt-6 text-center'>
                      <p>
                        Don’t have any account? &nbsp;
                        <Link to='/auth/register' className='text-primary'>
                          Register
                        </Link>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Login;
