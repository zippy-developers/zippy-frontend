import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import LogoDark from '../../images/logo/logo-dark.svg';
import Logo from '../../images/logo/logo.svg';
import toast from 'react-hot-toast';
import axios from 'axios';
import { useAuth } from '../../context/AuthContext';
import { validateStep } from './formValidation';
import Loader from '../../common/Loader';

const apiUrl = import.meta.env.VITE_API_URL;

const Register = () => {
  const navigate = useNavigate();
  const [step, setStep] = useState(1);
  const [loading, setLoading] = useState(false);
  const { dispatch } = useAuth();
  const [errors, setErrors] = useState({});
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    password: '',
    phoneNumber: '',
    address: '',
    role: '',
    isActive: '',
  });

  const handleInputChange = (e: any) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));

    // Clear the error message for the changed field
    setErrors((prevErrors) => ({
      ...prevErrors,
      [name]: '',
    }));
  };

  const nextStep = async () => {
    const { fieldErrors, valid } = await validateStep(step, formData);
    if (valid) {
      if (step < 3) {
        setStep(step + 1);
      } else {
        try {
          setLoading(true);
          const userRole = 'Customer';
          const isActive = true;
          const formDataWithRole = {
            ...formData,
            role: userRole,
            isActive: isActive,
          };

          const response = await axios.post(
            `${apiUrl}/auth/register`,
            formDataWithRole,
            {
              headers: {
                'Content-Type': 'application/json',
              },
            }
          );

          if (response.status === 201) {
            toast.success('User registered successfully');

            // Automatically log in the user after registration
            const loginResponse = await axios.post(`${apiUrl}/auth/login`, {
              username: formData.username,
              password: formData.password,
            });

            if (loginResponse.status === 200) {
              const user = loginResponse.data;
              dispatch({ type: 'LOGIN_SUCCESS', payload: user });
              navigate('/');
            } else {
              toast.error('Error logging in user after registration');
              console.error(
                'Error logging in user after registration:',
                loginResponse.status
              );
            }
          } else {
            toast.error('Error registering user');
            console.error('Error registering user:', response.status);
          }
        } catch (error) {
          toast.error('An error occurred while registering user');
          console.error('An error occurred while registering user:', error);
        } finally {
          setLoading(false);
        }
      }
    }
  };

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <div className='mx-auto flex flex-col items-center justify-center bg-primary min-[420px]:h-screen sm:h-screen md:h-screen lg:h-screen xl:h-screen'>
          <div className='rounded-xl border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark sm:w-[600px] md:w-[700px] lg:w-[1000px]'>
            <div className='flex flex-wrap items-center'>
              <div className='hidden w-full xl:block xl:w-1/2'>
                <div className='py-10 text-center px-26'>
                  <Link className='mb-5.5 inline-block' to='/'>
                    <img
                      className='hidden w-full dark:block'
                      src={LogoDark}
                      alt='Logo'
                    />
                    <img className='w-full dark:hidden' src={Logo} alt='Logo' />
                  </Link>
                  <p className='2xl:px-2'>
                    With a growing population, there is a growing need for more
                    delivery services.
                  </p>
                </div>
              </div>

              <div className='w-full border-stroke dark:border-strokedark xl:w-1/2 xl:border-l-2'>
                <div className='w-full p-4 sm:p-12.5 xl:p-17.5'>
                  <div className='mb-4 text-black dark:text-white'>
                    Step {step} of 3
                  </div>
                  <h2 className='text-2xl font-bold text-black mb-9 dark:text-white sm:text-title-xl2'>
                    Register to Zippy
                  </h2>

                  <form>
                    {step === 1 && (
                      <>
                        <div className='flex gap-2'>
                          <div className='mb-4'>
                            <label className='mb-2.5 block font-medium text-black dark:text-white'>
                              First Name
                            </label>
                            <div className='relative'>
                              <input
                                type='text'
                                id='firstName'
                                name='firstName'
                                placeholder='First name'
                                value={formData.firstName}
                                onChange={handleInputChange}
                                className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                              />
                            </div>
                          </div>
                          <div className='mb-4'>
                            <label className='mb-2.5 block font-medium text-black dark:text-white'>
                              Last Name
                            </label>
                            <div className='relative'>
                              <input
                                type='text'
                                id='lastName'
                                name='lastName'
                                value={formData.lastName}
                                onChange={handleInputChange}
                                placeholder='Last name'
                                className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                              />
                            </div>
                          </div>
                        </div>
                        <div className='mb-4'>
                          <label className='mb-2.5 block font-medium text-black dark:text-white'>
                            Username
                          </label>
                          <div className='relative'>
                            <input
                              type='text'
                              id='username'
                              name='username'
                              value={formData.username}
                              onChange={handleInputChange}
                              placeholder='Enter your username'
                              className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                            />
                          </div>
                        </div>
                      </>
                    )}

                    {step === 2 && (
                      <>
                        <div className='mb-4'>
                          <label className='mb-2.5 block font-medium text-black dark:text-white'>
                            Email
                          </label>
                          <div className='relative'>
                            <input
                              type='email'
                              id='email'
                              name='email'
                              value={formData.email}
                              onChange={handleInputChange}
                              placeholder='Enter your email'
                              className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                            />
                          </div>
                        </div>
                        <div className='mb-4'>
                          <label className='mb-2.5 block font-medium text-black dark:text-white'>
                            Password
                          </label>
                          <div className='relative'>
                            <input
                              type='password'
                              id='password'
                              name='password'
                              value={formData.password}
                              onChange={handleInputChange}
                              placeholder='Enter your password'
                              className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                            />
                          </div>
                        </div>
                      </>
                    )}

                    {step === 3 && (
                      <>
                        <div className='mb-4'>
                          <label className='mb-2.5 block font-medium text-black dark:text-white'>
                            Phone Number
                          </label>
                          <div className='relative'>
                            <input
                              type='text'
                              id='phoneNumber'
                              name='phoneNumber'
                              value={formData.phoneNumber}
                              onChange={handleInputChange}
                              placeholder='Enter your phone number'
                              className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                            />
                          </div>
                        </div>
                        <div className='mb-4'>
                          <label className='mb-2.5 block font-medium text-black dark:text-white'>
                            Address
                          </label>
                          <div className='relative'>
                            <input
                              type='text'
                              id='address'
                              name='address'
                              value={formData.address}
                              onChange={handleInputChange}
                              placeholder='Enter your address'
                              className='w-full py-4 pl-6 pr-10 bg-transparent border rounded-lg outline-none border-stroke focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                            />
                          </div>
                        </div>
                      </>
                    )}

                    <div className='mb-5'>
                      <button
                        type='button'
                        onClick={nextStep}
                        disabled={loading}
                        className='w-full p-4 text-white transition border rounded-lg cursor-pointer border-primary bg-primary hover:bg-opacity-90'
                      >
                        {step < 3 ? 'Next Step' : 'Sign Up'}
                      </button>
                    </div>

                    <div className='mt-6 text-center'>
                      <p>
                        Already have an account? &nbsp;
                        <Link to='/auth/login' className='text-primary'>
                          Login
                        </Link>
                      </p>
                    </div>
                    <div className='mt-4'>
                      <p className='text-xs'>
                        By joining, you agree to the Zippy &nbsp;
                        <Link to='/terms' className='text-primary'>
                          Terms of Condition
                        </Link>
                        &nbsp; and to occasionally receive emails from us. Please
                        read our &nbsp;
                        <Link to='/privacy-policy' className='text-primary'>
                          Privacy Policy
                        </Link>
                        &nbsp; to learn how we use your personal data.
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Register;
