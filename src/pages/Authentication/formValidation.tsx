import axios from 'axios';
import toast from 'react-hot-toast';

const apiUrl = import.meta.env.VITE_API_URL;

export const validateStep = async (step: any, formData: any) => {
  const fieldErrors: any = {};
  if (step === 1) {
    if (!formData.firstName) {
      fieldErrors.firstName = 'First name is required';
      toast.error('First name is required');
    }
    if (!formData.lastName) {
      fieldErrors.lastName = 'Last name is required';
      toast.error('Last name is required');
    }
    if (!formData.username) {
      fieldErrors.username = 'Username is required';
      toast.error('Username is required');
    } else {
      try {
        const response = await axios.get(
          `${apiUrl}/auth/username/${formData.username}`
        );
        if (!response.data.available) {
          fieldErrors.username = 'Username already exists';
          toast.error('Username already exists');
        }
      } catch (error) {
        fieldErrors.username = 'Error checking username';
        toast.error('Error checking username');
      }
    }
  } else if (step === 2) {
    if (!formData.email) {
      fieldErrors.email = 'Email is required';
      toast.error('Email is required');
    } else if (
      !/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(formData.email)
    ) {
      fieldErrors.email = 'Invalid email format';
      toast.error('Invalid email format');
    } else {
      try {
        const response = await axios.get(
          `${apiUrl}/auth/email/${formData.email}`
        );
        if (!response.data.available) {
          fieldErrors.email = 'Email already exists';
          toast.error('Email already exists');
        }
      } catch (error) {
        toast.error('Error checking email');
        fieldErrors.email = 'Error checking email';
        console.error('Error checking email:', error);
      }
    }
    if (!formData.password) {
      fieldErrors.password = 'Password is required';
      toast.error('Password is required');
    } else if (formData.password.length < 8) {
      fieldErrors.password = 'Password must be at least 8 characters long';
      toast.error('Password must be at least 8 characters long');
    }
  } else if (step === 3) {
    if (!formData.phoneNumber) {
      fieldErrors.phoneNumber = 'Phone number is required';
      toast.error('Phone number is required');
    }
    if (!formData.address) {
      fieldErrors.address = 'Address is required';
      toast.error('Address is required');
    }
  }

  return {
    fieldErrors,
    valid: Object.keys(fieldErrors).length === 0,
  };
};
