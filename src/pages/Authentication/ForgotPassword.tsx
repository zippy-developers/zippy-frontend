import { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import toast from 'react-hot-toast';

const apiUrl = import.meta.env.VITE_API_URL;

const ForgotPassword = () => {
  const [email, setEmail] = useState('');

  const handleChange = (e: any) => {
    setEmail(e.target.value);
  };

  axios.defaults.withCredentials = true;

  const handleResetPassword = async (e: any) => {
    e.preventDefault();

    try {
      await axios.post(`${apiUrl}/auth/forgot-password`, {
        email,
      });
      toast.success('Password reset instructions sent to your email.');
    } catch (error) {
      toast.error('Password reset failed. Please try again.');
      console.error('Password reset failed. Please try again.', error);
    }
  };

  return (
    <div className='min-[420px] mx-auto flex h-screen flex-col items-center justify-center bg-primary'>
      <div className='w-full rounded-xl border border-stroke bg-white shadow-default dark:border-strokedark dark:bg-boxdark xsm:w-[300px] sm:w-[400px] md:w-[400px] lg:w-[500px]'>
        <div className='w-full p-4 sm:p-12.5 xl:p-17.5'>
          <h2 className='mb-9 text-2xl font-bold text-black dark:text-white sm:text-title-xl2'>
            Forgot Password
          </h2>

          <form>
            <div className='mb-4'>
              <label className='mb-2.5 block font-medium text-black dark:text-white'>
                Email
              </label>
              <div className='relative'>
                <input
                  type='email'
                  value={email}
                  onChange={handleChange}
                  placeholder='Enter your email'
                  className='w-full rounded-lg border border-stroke bg-transparent py-4 pl-6 pr-10 outline-none focus:border-primary focus-visible:shadow-none dark:border-form-strokedark dark:bg-form-input dark:focus:border-primary'
                />
              </div>
            </div>

            <div className='mb-5'>
              <button
                onClick={handleResetPassword}
                type='submit'
                value='Reset Password'
                className='w-full cursor-pointer rounded-lg border border-primary bg-primary p-4 text-white transition hover:bg-opacity-90'
              >
                Reset Password
              </button>
            </div>

            <p className='mt-6 text-center'>
              Remember your password?{' '}
              <Link to='/auth/login' className='text-primary'>
                Login
              </Link>
            </p>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ForgotPassword;
