import React from 'react';
import { RxArrowLeft } from 'react-icons/rx';

const PrivacyPolicy = () => {
  return (
    <div className='mx-auto flex flex-col items-center justify-center bg-primary py-20'>
      <div>
        <div className='flex cursor-pointer items-center justify-end gap-2 py-5 text-white'>
          <span>
            <RxArrowLeft />
          </span>
          <button
            type='button'
            onClick={() => window.history.back() as any}
            className='rounded-lg border border-primary bg-primary  text-white transition hover:bg-opacity-90'
          >
            Back
          </button>
        </div>
        <div className='rounded-xl border border-stroke bg-white p-18 shadow-default dark:border-strokedark dark:bg-boxdark sm:w-auto md:w-auto lg:w-auto'>
          <div className='mx-auto max-w-3xl text-black dark:text-white'>
            <h1 className='mb-4 text-3xl font-bold'>
              Zippy Delivery Service - Privacy Policy
            </h1>
            <p>Effective Date: 2023/10/12</p>

            <p className='mt-4'>
              <strong>1. Information We Collect</strong>
            </p>
            <p>
              We may collect personal information when you use our website,
              including but not limited to your name, contact information, and
              account details.
            </p>

            <p className='mt-4'>
              <strong>2. How We Use Your Information</strong>
            </p>
            <p>
              We use your information to provide and improve our services,
              process delivery requests, and communicate with you about your
              account.
            </p>

            <p className='mt-4'>
              <strong>3. Information Sharing</strong>
            </p>
            <p>
              We may share your information with third-party service providers
              who help us deliver our services. We do not sell your information
              to third parties.
            </p>

            <p className='mt-4'>
              <strong>4. Data Security</strong>
            </p>
            <p>
              We take reasonable measures to protect your information, but no
              data transmission or storage is entirely secure. You are
              responsible for maintaining the security of your account
              credentials.
            </p>

            <p className='mt-4'>
              <strong>5. Your Choices</strong>
            </p>
            <p>
              You can update your account information and preferences. You may
              also request to delete your account.
            </p>

            <p className='mt-4'>
              <strong>6. Changes to the Privacy Policy</strong>
            </p>
            <p>
              We may update this Privacy Policy to reflect changes in our
              practices and services. We will notify you of significant changes.
              You should periodically review this policy.
            </p>

            <p className='mt-4'>
              <strong>7. Contact Information</strong>
            </p>
            <p>
              If you have questions or concerns about our Privacy Policy, please
              contact us at contact@zippy.lk.
            </p>

            <p>
              By using Zippy Delivery Service, you acknowledge that you have
              read, understood, and agree to our Privacy Policy.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrivacyPolicy;
